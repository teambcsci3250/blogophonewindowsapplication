﻿namespace BlogophoneWindowsApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DefaultNewLoginPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LoginFromDefaultPanelButton = new System.Windows.Forms.Button();
            this.RegisterFromDefaultPanelButton = new System.Windows.Forms.Button();
            this.DefaultLoginPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.LoginAttemptButton = new System.Windows.Forms.Button();
            this.CancelLoginButton = new System.Windows.Forms.Button();
            this.NewRegisterPanel = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.RegisterAttemptButton = new System.Windows.Forms.Button();
            this.CancelRegisterButton = new System.Windows.Forms.Button();
            this.HomePanel = new System.Windows.Forms.Panel();
            this.ProfileNameLabel = new System.Windows.Forms.Label();
            this.NewBlogButton = new System.Windows.Forms.Button();
            this.ViewBlogsButton = new System.Windows.Forms.Button();
            this.NewBlogPanel = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SubmitNewBlogButton = new System.Windows.Forms.Button();
            this.CancelNewBlogButton = new System.Windows.Forms.Button();
            this.ViewBlogsPanel = new System.Windows.Forms.Panel();
            this.HomeFromViewBlogButton = new System.Windows.Forms.Button();
            this.DefaultNewLoginPanel.SuspendLayout();
            this.DefaultLoginPanel.SuspendLayout();
            this.NewRegisterPanel.SuspendLayout();
            this.HomePanel.SuspendLayout();
            this.NewBlogPanel.SuspendLayout();
            this.ViewBlogsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DefaultNewLoginPanel
            // 
            this.DefaultNewLoginPanel.Controls.Add(this.RegisterFromDefaultPanelButton);
            this.DefaultNewLoginPanel.Controls.Add(this.LoginFromDefaultPanelButton);
            this.DefaultNewLoginPanel.Controls.Add(this.label2);
            this.DefaultNewLoginPanel.Controls.Add(this.label1);
            this.DefaultNewLoginPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DefaultNewLoginPanel.Location = new System.Drawing.Point(0, 0);
            this.DefaultNewLoginPanel.Name = "DefaultNewLoginPanel";
            this.DefaultNewLoginPanel.Size = new System.Drawing.Size(346, 393);
            this.DefaultNewLoginPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(41, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(263, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "It appears this is your first time  loading this application.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(79, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Do you already have an account?";
            // 
            // LoginFromDefaultPanelButton
            // 
            this.LoginFromDefaultPanelButton.Location = new System.Drawing.Point(44, 182);
            this.LoginFromDefaultPanelButton.Name = "LoginFromDefaultPanelButton";
            this.LoginFromDefaultPanelButton.Size = new System.Drawing.Size(260, 36);
            this.LoginFromDefaultPanelButton.TabIndex = 2;
            this.LoginFromDefaultPanelButton.Text = "Yes, Log in";
            this.LoginFromDefaultPanelButton.UseVisualStyleBackColor = true;
            this.LoginFromDefaultPanelButton.Click += new System.EventHandler(this.LoginFromDefaultPanel_Click);
            // 
            // RegisterFromDefaultPanelButton
            // 
            this.RegisterFromDefaultPanelButton.Location = new System.Drawing.Point(44, 258);
            this.RegisterFromDefaultPanelButton.Name = "RegisterFromDefaultPanelButton";
            this.RegisterFromDefaultPanelButton.Size = new System.Drawing.Size(260, 36);
            this.RegisterFromDefaultPanelButton.TabIndex = 3;
            this.RegisterFromDefaultPanelButton.Text = "No, register a new account";
            this.RegisterFromDefaultPanelButton.UseVisualStyleBackColor = true;
            this.RegisterFromDefaultPanelButton.Click += new System.EventHandler(this.RegisterFromDefaultPanelButton_Click);
            // 
            // DefaultLoginPanel
            // 
            this.DefaultLoginPanel.Controls.Add(this.CancelLoginButton);
            this.DefaultLoginPanel.Controls.Add(this.LoginAttemptButton);
            this.DefaultLoginPanel.Controls.Add(this.textBox2);
            this.DefaultLoginPanel.Controls.Add(this.textBox1);
            this.DefaultLoginPanel.Controls.Add(this.label5);
            this.DefaultLoginPanel.Controls.Add(this.label4);
            this.DefaultLoginPanel.Controls.Add(this.label3);
            this.DefaultLoginPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DefaultLoginPanel.Location = new System.Drawing.Point(0, 0);
            this.DefaultLoginPanel.Name = "DefaultLoginPanel";
            this.DefaultLoginPanel.Size = new System.Drawing.Size(346, 393);
            this.DefaultLoginPanel.TabIndex = 4;
            this.DefaultLoginPanel.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(115, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Welcome back!";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Username";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Password";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(204, 100);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(204, 163);
            this.textBox2.Name = "textBox2";
            this.textBox2.PasswordChar = '*';
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 4;
            // 
            // LoginAttemptButton
            // 
            this.LoginAttemptButton.Location = new System.Drawing.Point(44, 258);
            this.LoginAttemptButton.Name = "LoginAttemptButton";
            this.LoginAttemptButton.Size = new System.Drawing.Size(75, 23);
            this.LoginAttemptButton.TabIndex = 5;
            this.LoginAttemptButton.Text = "Log in";
            this.LoginAttemptButton.UseVisualStyleBackColor = true;
            this.LoginAttemptButton.Click += new System.EventHandler(this.LoginAttemptButton_Click);
            // 
            // CancelLoginButton
            // 
            this.CancelLoginButton.Location = new System.Drawing.Point(229, 258);
            this.CancelLoginButton.Name = "CancelLoginButton";
            this.CancelLoginButton.Size = new System.Drawing.Size(75, 23);
            this.CancelLoginButton.TabIndex = 6;
            this.CancelLoginButton.Text = "Cancel";
            this.CancelLoginButton.UseVisualStyleBackColor = true;
            this.CancelLoginButton.Click += new System.EventHandler(this.CancelLoginButton_Click);
            // 
            // NewRegisterPanel
            // 
            this.NewRegisterPanel.Controls.Add(this.CancelRegisterButton);
            this.NewRegisterPanel.Controls.Add(this.RegisterAttemptButton);
            this.NewRegisterPanel.Controls.Add(this.label11);
            this.NewRegisterPanel.Controls.Add(this.textBox7);
            this.NewRegisterPanel.Controls.Add(this.textBox6);
            this.NewRegisterPanel.Controls.Add(this.textBox5);
            this.NewRegisterPanel.Controls.Add(this.textBox4);
            this.NewRegisterPanel.Controls.Add(this.textBox3);
            this.NewRegisterPanel.Controls.Add(this.label10);
            this.NewRegisterPanel.Controls.Add(this.label9);
            this.NewRegisterPanel.Controls.Add(this.label8);
            this.NewRegisterPanel.Controls.Add(this.label7);
            this.NewRegisterPanel.Controls.Add(this.label6);
            this.NewRegisterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NewRegisterPanel.Location = new System.Drawing.Point(0, 0);
            this.NewRegisterPanel.Name = "NewRegisterPanel";
            this.NewRegisterPanel.Size = new System.Drawing.Size(346, 393);
            this.NewRegisterPanel.TabIndex = 7;
            this.NewRegisterPanel.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Username";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Password";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 258);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Password Confirm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(44, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Email";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(204, 56);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 5;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(204, 100);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 6;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(204, 150);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 7;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(204, 202);
            this.textBox6.Name = "textBox6";
            this.textBox6.PasswordChar = '*';
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 8;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(204, 255);
            this.textBox7.Name = "textBox7";
            this.textBox7.PasswordChar = '*';
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(79, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(179, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Thank you for downloading this app.";
            // 
            // RegisterAttemptButton
            // 
            this.RegisterAttemptButton.Location = new System.Drawing.Point(44, 300);
            this.RegisterAttemptButton.Name = "RegisterAttemptButton";
            this.RegisterAttemptButton.Size = new System.Drawing.Size(75, 23);
            this.RegisterAttemptButton.TabIndex = 11;
            this.RegisterAttemptButton.Text = "Register";
            this.RegisterAttemptButton.UseVisualStyleBackColor = true;
            this.RegisterAttemptButton.Click += new System.EventHandler(this.RegisterAttemptButton_Click);
            // 
            // CancelRegisterButton
            // 
            this.CancelRegisterButton.Location = new System.Drawing.Point(229, 300);
            this.CancelRegisterButton.Name = "CancelRegisterButton";
            this.CancelRegisterButton.Size = new System.Drawing.Size(75, 23);
            this.CancelRegisterButton.TabIndex = 12;
            this.CancelRegisterButton.Text = "Cancel";
            this.CancelRegisterButton.UseVisualStyleBackColor = true;
            this.CancelRegisterButton.Click += new System.EventHandler(this.CancelRegisterButton_Click);
            // 
            // HomePanel
            // 
            this.HomePanel.Controls.Add(this.ViewBlogsButton);
            this.HomePanel.Controls.Add(this.NewBlogButton);
            this.HomePanel.Controls.Add(this.ProfileNameLabel);
            this.HomePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HomePanel.Location = new System.Drawing.Point(0, 0);
            this.HomePanel.Name = "HomePanel";
            this.HomePanel.Size = new System.Drawing.Size(346, 393);
            this.HomePanel.TabIndex = 13;
            this.HomePanel.Visible = false;
            // 
            // ProfileNameLabel
            // 
            this.ProfileNameLabel.AutoSize = true;
            this.ProfileNameLabel.Location = new System.Drawing.Point(13, 13);
            this.ProfileNameLabel.Name = "ProfileNameLabel";
            this.ProfileNameLabel.Size = new System.Drawing.Size(67, 13);
            this.ProfileNameLabel.TabIndex = 0;
            this.ProfileNameLabel.Text = "Profile Name";
            // 
            // NewBlogButton
            // 
            this.NewBlogButton.Location = new System.Drawing.Point(47, 59);
            this.NewBlogButton.Name = "NewBlogButton";
            this.NewBlogButton.Size = new System.Drawing.Size(242, 57);
            this.NewBlogButton.TabIndex = 1;
            this.NewBlogButton.Text = "New Blog";
            this.NewBlogButton.UseVisualStyleBackColor = true;
            this.NewBlogButton.Click += new System.EventHandler(this.NewBlogButton_Click);
            // 
            // ViewBlogsButton
            // 
            this.ViewBlogsButton.Location = new System.Drawing.Point(47, 165);
            this.ViewBlogsButton.Name = "ViewBlogsButton";
            this.ViewBlogsButton.Size = new System.Drawing.Size(242, 57);
            this.ViewBlogsButton.TabIndex = 2;
            this.ViewBlogsButton.Text = "View Blogs";
            this.ViewBlogsButton.UseVisualStyleBackColor = true;
            this.ViewBlogsButton.Click += new System.EventHandler(this.ViewBlogsButton_Click);
            // 
            // NewBlogPanel
            // 
            this.NewBlogPanel.Controls.Add(this.CancelNewBlogButton);
            this.NewBlogPanel.Controls.Add(this.SubmitNewBlogButton);
            this.NewBlogPanel.Controls.Add(this.richTextBox1);
            this.NewBlogPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NewBlogPanel.Location = new System.Drawing.Point(0, 0);
            this.NewBlogPanel.Name = "NewBlogPanel";
            this.NewBlogPanel.Size = new System.Drawing.Size(346, 393);
            this.NewBlogPanel.TabIndex = 3;
            this.NewBlogPanel.Visible = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(47, 28);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(242, 155);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // SubmitNewBlogButton
            // 
            this.SubmitNewBlogButton.Location = new System.Drawing.Point(47, 257);
            this.SubmitNewBlogButton.Name = "SubmitNewBlogButton";
            this.SubmitNewBlogButton.Size = new System.Drawing.Size(75, 23);
            this.SubmitNewBlogButton.TabIndex = 1;
            this.SubmitNewBlogButton.Text = "Submit";
            this.SubmitNewBlogButton.UseVisualStyleBackColor = true;
            this.SubmitNewBlogButton.Click += new System.EventHandler(this.SubmitNewBlogButton_Click);
            // 
            // CancelNewBlogButton
            // 
            this.CancelNewBlogButton.Location = new System.Drawing.Point(214, 258);
            this.CancelNewBlogButton.Name = "CancelNewBlogButton";
            this.CancelNewBlogButton.Size = new System.Drawing.Size(75, 23);
            this.CancelNewBlogButton.TabIndex = 2;
            this.CancelNewBlogButton.Text = "Cancel";
            this.CancelNewBlogButton.UseVisualStyleBackColor = true;
            this.CancelNewBlogButton.Click += new System.EventHandler(this.CancelNewBlogButton_Click);
            // 
            // ViewBlogsPanel
            // 
            this.ViewBlogsPanel.Controls.Add(this.HomeFromViewBlogButton);
            this.ViewBlogsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ViewBlogsPanel.Location = new System.Drawing.Point(0, 0);
            this.ViewBlogsPanel.Name = "ViewBlogsPanel";
            this.ViewBlogsPanel.Size = new System.Drawing.Size(346, 393);
            this.ViewBlogsPanel.TabIndex = 3;
            this.ViewBlogsPanel.Visible = false;
            // 
            // HomeFromViewBlogButton
            // 
            this.HomeFromViewBlogButton.Location = new System.Drawing.Point(118, 300);
            this.HomeFromViewBlogButton.Name = "HomeFromViewBlogButton";
            this.HomeFromViewBlogButton.Size = new System.Drawing.Size(75, 23);
            this.HomeFromViewBlogButton.TabIndex = 0;
            this.HomeFromViewBlogButton.Text = "Home";
            this.HomeFromViewBlogButton.UseVisualStyleBackColor = true;
            this.HomeFromViewBlogButton.Click += new System.EventHandler(this.HomeFromViewBlogButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 393);
            this.Controls.Add(this.ViewBlogsPanel);
            this.Controls.Add(this.NewBlogPanel);
            this.Controls.Add(this.HomePanel);
            this.Controls.Add(this.NewRegisterPanel);
            this.Controls.Add(this.DefaultLoginPanel);
            this.Controls.Add(this.DefaultNewLoginPanel);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.DefaultNewLoginPanel.ResumeLayout(false);
            this.DefaultNewLoginPanel.PerformLayout();
            this.DefaultLoginPanel.ResumeLayout(false);
            this.DefaultLoginPanel.PerformLayout();
            this.NewRegisterPanel.ResumeLayout(false);
            this.NewRegisterPanel.PerformLayout();
            this.HomePanel.ResumeLayout(false);
            this.HomePanel.PerformLayout();
            this.NewBlogPanel.ResumeLayout(false);
            this.ViewBlogsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel DefaultNewLoginPanel;
        private System.Windows.Forms.Button LoginFromDefaultPanelButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button RegisterFromDefaultPanelButton;
        private System.Windows.Forms.Panel DefaultLoginPanel;
        private System.Windows.Forms.Button LoginAttemptButton;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button CancelLoginButton;
        private System.Windows.Forms.Panel NewRegisterPanel;
        private System.Windows.Forms.Button CancelRegisterButton;
        private System.Windows.Forms.Button RegisterAttemptButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel HomePanel;
        private System.Windows.Forms.Button ViewBlogsButton;
        private System.Windows.Forms.Button NewBlogButton;
        private System.Windows.Forms.Label ProfileNameLabel;
        private System.Windows.Forms.Panel NewBlogPanel;
        private System.Windows.Forms.Button CancelNewBlogButton;
        private System.Windows.Forms.Button SubmitNewBlogButton;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel ViewBlogsPanel;
        private System.Windows.Forms.Button HomeFromViewBlogButton;
    }
}

