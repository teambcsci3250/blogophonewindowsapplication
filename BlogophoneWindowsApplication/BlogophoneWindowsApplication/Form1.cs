﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlogophoneWindowsApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void LoginFromDefaultPanel_Click(object sender, EventArgs e)
        {
            //default -> login
            DefaultNewLoginPanel.Visible = false;
            DefaultLoginPanel.Visible = true;
        }

        private void RegisterFromDefaultPanelButton_Click(object sender, EventArgs e)
        {
            //default -> register
            DefaultNewLoginPanel.Visible = false;
            NewRegisterPanel.Visible = true;
        }

        private void LoginAttemptButton_Click(object sender, EventArgs e)
        {
            //Login -> Home
            DefaultLoginPanel.Visible = false;
            HomePanel.Visible = true;
        }

        private void CancelLoginButton_Click(object sender, EventArgs e)
        {
            //Login - Default
            DefaultLoginPanel.Visible = false;
            DefaultNewLoginPanel.Visible = true;
        }

        private void CancelRegisterButton_Click(object sender, EventArgs e)
        {
            //Register - Default
            NewRegisterPanel.Visible = false;
            DefaultNewLoginPanel.Visible = true;
        }

        private void RegisterAttemptButton_Click(object sender, EventArgs e)
        {
            //Register - Home
            NewRegisterPanel.Visible = false;
            HomePanel.Visible = true;
        }

        private void NewBlogButton_Click(object sender, EventArgs e)
        {
            //Home -> New Blog
            HomePanel.Visible = false;
            NewBlogPanel.Visible = true;
        }

        private void ViewBlogsButton_Click(object sender, EventArgs e)
        {
            //Home -> View Blog
            HomePanel.Visible = false;
            ViewBlogsPanel.Visible = true;
        }

        private void SubmitNewBlogButton_Click(object sender, EventArgs e)
        {
            //new blog -> Home
            NewBlogPanel.Visible = false;
            HomePanel.Visible = true;
        }

        private void CancelNewBlogButton_Click(object sender, EventArgs e)
        {
            //new blog -> Home
            NewBlogPanel.Visible = false;
            HomePanel.Visible = true;
        }

        private void HomeFromViewBlogButton_Click(object sender, EventArgs e)
        {
            //view blog -> Home
            ViewBlogsPanel.Visible = false;
            HomePanel.Visible = true;
        }
    }
}
